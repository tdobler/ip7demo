package ch.fhnw.thomasdobler.ch.ip7.learning;

import java.util.*;

public class LinearReg_GradientDescent {

	// Training instances list
	private List<double[]> trainingExamples = new LinkedList<double[]>();
	// Size of training set
	private static final int sizeTrainingset = 10000;
	// Initial parameter of theta
	private double[] theta = { 1, 1, 1, 1 };
	// Configurable compoenent of step size during theata update
	private final double alpha = 0.00000001;
	// Amount of iteration in Batch Gradient Descent
	private static final int iterations = 10000;
	// Print out configuration
	private static final int printsAtStartAndEnd = 5;

	private void buildTrainingExample(int amount) {

		// Area of the house
		double areaMin = 80;
		double areaMax = 1000;
		double areaRange = areaMax - areaMin;

		// Distance to center
		double distanceMin = 10;
		double distanceMax = 10000;
		double distanceRange = distanceMax - distanceMin;

		// Year of construction
		double yocMin = 1950;
		double yocMax = 2015;
		double yocRange = yocMax - yocMin;

		// Generate training examples with prices
		for (int i = 0; i < amount; i++) {
			double[] example = new double[5];
			example[0] = 1.0;
			example[1] = areaMin + Math.random() * areaRange;
			example[2] = distanceMin + Math.random() * distanceRange;
			example[3] = yocMin + Math.random() * yocRange;
			// Price depends on prior attributes
			double price = 0;
			price += _priceComponent(example[1], areaRange);
			price += _priceComponent(example[2], distanceRange);
			price += _priceComponent(example[3], yocRange);
			example[4] = price;
			// Add generated training instance to training set
			trainingExamples.add(example);
		}
	}

	// Random price depends a bit on prior attributes
	private double _priceComponent(double value, double range) {
		if (value <= range / 3)
			return 50000 + 50000 * Math.random() * 0.1;
		if (value <= (range / 3 * 2))
			return 100000 + 100000 * Math.random() * 0.1;
		return 150000 + 150000 * Math.random() * 0.1;
	}

	// Hypothesis Implementation
	private double priceCalculationByHypothesis(double[] features) {
		return this.theta[0] * features[0] + this.theta[1] * features[1] + this.theta[2] * features[2]
				+ this.theta[3] * features[3];
	}

	// Costfunction: Mean squared error function
	private double gradientBatch_costs() {
		double costs = this.trainingExamples.stream()
				.mapToDouble(l -> Math.pow((priceCalculationByHypothesis(l) - l[4]), 2.0)).sum();
		return costs / this.trainingExamples.size() / 2;
	}

	// Theta Update with Batch Gradient Descent
	private void gradientBatch_thetaUpdate(int amount) {
		for (int i = 0; i < amount; i++) {
			double partialDerivative0 = this.trainingExamples.stream()
					.mapToDouble(l -> (priceCalculationByHypothesis(l) - l[4]) * l[0]).sum();
			double tmpTheta0 = this.theta[0] - (this.alpha * partialDerivative0 / this.trainingExamples.size());
			double partialDerivative1 = this.trainingExamples.stream()
					.mapToDouble(l -> (priceCalculationByHypothesis(l) - l[4]) * l[1]).sum();
			double tmpTheta1 = this.theta[1] - (this.alpha * partialDerivative1 / this.trainingExamples.size());
			double partialDerivative2 = this.trainingExamples.stream()
					.mapToDouble(l -> (priceCalculationByHypothesis(l) - l[4]) * l[2]).sum();
			double tmpTheta2 = this.theta[2] - (this.alpha * partialDerivative2 / this.trainingExamples.size());
			double partialDerivative3 = this.trainingExamples.stream()
					.mapToDouble(l -> (priceCalculationByHypothesis(l) - l[4]) * l[3]).sum();
			double tmpTheta3 = this.theta[3] - (this.alpha * partialDerivative3 / this.trainingExamples.size());
			// Must happen synchronously
			this.theta = new double[] { tmpTheta0, tmpTheta1, tmpTheta2, tmpTheta3 };
		}
	}

	// Theta update with Stochastic Gradient Descent
	private void gradientStochastic_thetaUpdate(double[] feature) {
		double tmpTheta0 = this.theta[0]
				- this.alpha * (priceCalculationByHypothesis(feature) - feature[4]) * feature[0];
		double tmpTheta1 = this.theta[1]
				- this.alpha * (priceCalculationByHypothesis(feature) - feature[4]) * feature[1];
		double tmpTheta2 = this.theta[2]
				- this.alpha * (priceCalculationByHypothesis(feature) - feature[4]) * feature[2];
		double tmpTheta3 = this.theta[3]
				- this.alpha * (priceCalculationByHypothesis(feature) - feature[4]) * feature[3];
		this.theta = new double[] { tmpTheta0, tmpTheta1, tmpTheta2, tmpTheta3 };
	}

	// Reset theta before stochastic gradient descent starts
	private void resetTheta() {
		this.theta = new double[] { 1, 1, 1, 1 };
	}

	private void printSummary(int iteration) {
		System.out.println(String.format("%s \t\t Theta: %f \t %f \t %f \t %f \t Costs: %f", iteration, this.theta[0],
				this.theta[1], this.theta[2], this.theta[3], this.gradientBatch_costs()));
	}

	public static void main(String[] args) {
		LinearReg_GradientDescent d = new LinearReg_GradientDescent();

		// Batch and Stochastic Gradient Descent use the same training example
		d.buildTrainingExample(sizeTrainingset);
		System.out.println("Batch Gradient Descent");
		// Print iteration zero
		d.printSummary(0);

		// Print first <printsAtStartAndEnd> iterations
		System.out.println(String.format("First %s iterations", printsAtStartAndEnd));
		for (int i = 1; i <= printsAtStartAndEnd; i++) {
			d.gradientBatch_thetaUpdate(1);
			d.printSummary(i);
		}

		// Calculate without an output
		d.gradientBatch_thetaUpdate(iterations - 2 * printsAtStartAndEnd);

		// Print last <printsAtStartAndEnd> iterations
		System.out.println(String.format("Last %s iterations", printsAtStartAndEnd));
		for (int i = iterations - printsAtStartAndEnd + 1; i <= iterations; i++) {
			d.gradientBatch_thetaUpdate(1);
			d.printSummary(i);
		}

		// Reset theta for stochastic gradient descent
		d.resetTheta();

		System.out.println("Stochastic Gradient Descent");
		d.printSummary(0);

		for (int i = 1; i <= printsAtStartAndEnd; i++) {
			d.gradientStochastic_thetaUpdate(d.trainingExamples.get(i - 1));
			d.printSummary(i);
		}

		for (int i = printsAtStartAndEnd; i <= sizeTrainingset - printsAtStartAndEnd; i++) {
			d.gradientStochastic_thetaUpdate(d.trainingExamples.get(i - 1));
			// d.printSummary(i);
		}

		for (int i = sizeTrainingset - printsAtStartAndEnd; i <= sizeTrainingset; i++) {
			d.gradientStochastic_thetaUpdate(d.trainingExamples.get(i - 1));
			d.printSummary(i);
		}

	}
}
