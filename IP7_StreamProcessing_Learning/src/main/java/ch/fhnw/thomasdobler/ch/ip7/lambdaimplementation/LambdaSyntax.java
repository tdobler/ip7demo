package ch.fhnw.thomasdobler.ch.ip7.lambdaimplementation;

import java.io.File;
import java.io.FileFilter;

public class LambdaSyntax {

	public static void main(String[] args) {
		// File object reference
		File directory = new File("/");

		// Approach 1
		// Without lambda expression
		File[] files1 = directory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isFile();
			}
		});

		// Approach 2
		// Assigns the FileFilter implementation as a lambda expression to the
		// variable myFilter
		FileFilter myFilter = (File f) -> {
			return f.isFile();
		};
		File[] files2 = directory.listFiles(myFilter);

		// Approach 3
		// Passes the FileFilter implementation as a lambda expression directly
		// as function parameter
		File[] files3 = directory.listFiles((File f) -> {
			return f.isFile();
		});

		// Approach 4
		// Shorter lambda syntax
		File[] files4 = directory.listFiles(f -> f.isFile());

		// Approach 5
		// Even more shorter lambda syntax
		File[] files5 = directory.listFiles(File::isFile);
	}

}
