package ch.fhnw.thomasdobler.ip7.storm.bolt;

import java.util.Map;

import org.apache.storm.Config;
import org.apache.storm.Constants;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class AverageBolt extends BaseBasicBolt {

	private long total = 0;
	private long amount = 0;

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {

		if (isTickTuple(tuple)) {
			collector.emit(new Values("Average: " + total / amount));
		} else {
			amount++;
			total += Integer.toUnsignedLong((int) tuple.getValue(0));
			collector.emit(new Values(tuple.getValue(0)));
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("value"));
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config conf = new Config();
		conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 3);
		return conf;
	}

	private static boolean isTickTuple(Tuple tuple) {
		return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
				&& tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
	}

}
