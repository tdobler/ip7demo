package ch.fhnw.thomasdobler.ip7.storm.topology;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.Utils;

import ch.fhnw.thomasdobler.ip7.storm.bolt.AlarmBolt;
import ch.fhnw.thomasdobler.ip7.storm.bolt.AverageBolt;
import ch.fhnw.thomasdobler.ip7.storm.bolt.FilterBolt;
import ch.fhnw.thomasdobler.ip7.storm.bolt.PrinterBolt;
import ch.fhnw.thomasdobler.ip7.storm.spout.RandomIntegerSpout;

public class SampleTopologyAssembling {
	public static void main(String[] args) {

		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("RandomIntegers", new RandomIntegerSpout());

		builder.setBolt("filter", new FilterBolt()).shuffleGrouping("RandomIntegers");
		builder.setBolt("average", new AverageBolt()).shuffleGrouping("filter");
		builder.setBolt("alarm", new AlarmBolt()).shuffleGrouping("filter");
		builder.setBolt("print", new PrinterBolt()).shuffleGrouping("alarm").shuffleGrouping("average");

		Config conf = new Config();

		LocalCluster cluster = new LocalCluster();

		cluster.submitTopology("IP7Demo", conf, builder.createTopology());
		Utils.sleep(60000);
		cluster.shutdown();
	}
}
